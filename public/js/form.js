var step = 1;



function extractStep() {
    data = $('#main-form #step' + step + ' :input').serializeArray();
    return data;
}

function validateStep(stepData) {
    if (isFirstStep()) {
	return validateFirstStep();
/*	
    } else if (isLastStep()) {
	return validateLastStep();
*/
    } else{
	console.log('validating');
	return true;
    }
}

function submitStep(stepData) {
    if (isLastStep()) {
	thankYou();
    } else {
	nextStep();
    }
}

function showErrors(errors) {
    errorString = '';
    
    $.each(errors, function(key, error) {
	errorString += key + ': ' + error + "\n";
    });
    
    alert(errorString);
}


function isLastStep() {
    return step == $('#main-form .step').length;
}

function isFirstStep() {
    return step == 1;
}

function nextStep() {
    $('#step' + step).addClass('hide');
    $('#step' + (step + 1)).removeClass('hide');
    
    step = step + 1;
}

function thankYou() {

    $('#main-form').addClass('hide');
    
    $('#thank-you').removeClass('hide');
}

function validateFirstStep() {
    errors = {};
    
    hasErrors = false;
    
    if ($('input[name="firstname"]').val() == '') {
	errors['Voornaam'] = 'niet ingevuld';
	hasErrors = true;
    }
    
    if ($('input[name="lastname"]').val() == '') {
	errors['Achternaam'] = 'niet ingevuld';
	hasErrors = true;
    }
    
    postcode = $('input[name="zipcode"]').val();
    postcodePattern = new RegExp('[0-9]{4}( )?[A-Za-z]{2}');
    if (postcode == '') {
	errors['Postcode'] = 'niet ingevuld';
	hasErrors = true;
    } else if( ! postcodePattern.test(postcode)) {
	errors['Postcode'] = 'is niet valide';
	hasErrors = true;
    }

    if ($('input[name="housenumber"]').val() == '') {
	errors['Huisnummer'] = 'niet ingevuld';
	hasErrors = true;
    } else if(isNaN($('input[name="housenumber"]').val())) {
	errors['Huisnummer'] = 'is geen geldig getal';
	hasErrors = true;
    }

    
    email = $('input[name="emailaddress"]').val();
    emailPattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
    
    if (email == '') {
	errors['E-mailadres'] = 'niet ingevuld';
	hasErrors = true;
    } else if( ! emailPattern.test(email)) {
	errors['E-mailadres'] = 'is niet valide';
	hasErrors = true;
    }
    
    if ($('input[name="optin"]').val() != '1') {
	errors['Voorwaarden en privacy'] = 'Niet aangevinkt';
	hasErrors = true;
    }
    
    if (hasErrors) {
	showErrors(errors);
	return false;
    } else {
	return true;
    }
}




module.exports = {
    extractStep : extractStep,
    validateStep : validateStep,
    submitStep : submitStep
}
