/*
 * All show actions are done in the back canvas, so are hidden until
 * canvas is swapped.
 */

var surveyCanvas     = false;
var surveyCanvasBack = false;
var currentQuestion  = false;

function surveyInit(selector, survey) {
    /* create canvasses */
    surveyCanvas     = $('<div />').addClass('canvas').addClass('canvas-front');
    surveyCanvasBack = $('<div />').addClass('canvas').addClass('canvas-back');
    $(selector).append(surveyCanvas);
    $(selector).append(surveyCanvasBack);
    
    surveyCanvas.css('opacity', 1);
    surveyCanvasBack.css('opacity', 0);

    /* show first question */
    if (typeof(survey.questions[0]) != "undefined") {
	currentQuestion = survey.questions[0];
	surveyQuestionShow(survey.questions[0]);
    }

    /* sets the why aren't you doing anything interval */
    
    setInterval(surveyIdleAnimate, 5000);
}

function surveyCanvasClear(back)
{
    if (typeof(back) == 'undefined') {
	back = true;
    }
    
    if (back) {
	surveyCanvasBack.html('');
	surveyCanvasBack.css('background', '#fff');
    } else {
	surveyCanvas.html('');
	surveyCanvas.css('background', '#fff');
    }
}

function surveySwap(duration) {
    console.log('swapping');
    surveyCanvas.removeClass('canvas-front');
    surveyCanvas.addClass('canvas-back');
    
    surveyCanvasBack.removeClass('canvas-back');
    surveyCanvasBack.addClass('canvas-front');
    
    surveyCanvas.animate({opacity: 0});
    surveyCanvasBack.animate({opacity: 1});

    /*
    var tempCanvas = surveyCanvasBack;
    surveyCanvasBack = surveyCanvas;
    surveyCanvas = tempCanvas;
    */
    surveyCanvasBack = $('.canvas-back');
    surveyCanvas = $('.canvas-front');
}

function surveyIdleAnimate() {
    /* animate chooser */
    if(
	typeof(currentQuestion.chooser.animation) != 'undefined' &&
	    $('.chooser').length &&
	    ! $('.chooser').hasClass('ui-draggable-dragging')
      ) {
	if (currentQuestion.chooser.animation == 'bounce') {
	    surveyEffect($('.chooser'), 'bounce');
	} else {
	    surveyEffect($('.chooser', currentQuestion.chooser.animation));
	}
    }

    /* animate arrows */
    $(currentQuestion.choices).each(function(i, choice) {
	var arrowClassName = 'choice-arrow-' + choice.position;
	
	if ($('.canvas-front .' + arrowClassName).length < 1) {
	    var arrow = $('<div />').addClass(arrowClassName).addClass('choice-arrow');
	    arrow.hide();
	    surveyCanvas.append(arrow);
	}
	
	//surveyEffect($('.' + arrowClassName), 'fade').fadeOut(1000);
	$('.' + arrowClassName)
	    .fadeIn(1000).fadeOut(1000)
	    .fadeIn(1000).fadeOut(1000)
	    //.remove()
	;
    });
}

function surveyEffect(target, effect)
{
    //target.effect(effect, {times:2}, 1000, function() {});
    var options = {};
    var time = 500;

    if (effect == 'bounce') {
	time = 1000;
	options['times'] = 2;
    } else if(effect == 'fade') {
	time = 1500;
	options['times'] = 1;
    }
    
    return target.effect(effect, options, time, function() {});
}

function surveyQuestionShow(question) {
    surveyCanvasBack.css('background-image', 'url("' + question.background + '")');

    if (question.intro) {
	intro = $('<h1 />')
	    .addClass('intro')
	    .html(question.intro)
	;
	surveyCanvasBack.append(intro);
    }

    if (question.text) {
	text = $('<div />')
	    .addClass('question-text')
	    .html(question.text)
	;
	surveyCanvasBack.append(text);
    }

    if(typeof(question.type) == 'undefined' || question.type == 'swipe') {
	$(question.choices).each(function(id, choice) {
	    surveyChoiceShow(choice);
	});
	
	surveyChooserShow(question.chooser);
    } else if(question.type == 'carousel') {
	surveyCarouselShow(question);
    }

    surveySwap();
}



/* -- carousel -- */

function surveyCarouselShow(question)
{
    //console.log(question);
    var carousel = $('<div />').attr('id', 'choice-carousel');
    $(question.choices).each(function(id, choice) {
	var choiceItem = surveyGenerateChoiceItem(choice);

	var carouselItem = $('<div />')
	    .addClass('carousel-item')
	    .append(choiceItem)
	    .click(function(e) {
		surveyChoiceMade($(e.target).data());
	    })
	;

	carousel.append(carouselItem);
    });
    
    surveyCanvasBack.append(carousel);
    carousel.slick({
	centerMode: true,
	centerPadding: '100px',
	infinite: true,
	//slidesToShow: 3,
	//slidesToScroll: 3,
	dots: true
    });
}


/* -- chooser -- */

function surveyChooserShow(chooser)
{
    img = $('<img />')
	.attr('src', chooser.image)
	.load(function() {
	    surveyChooserApplyWidth();
	})
    ;

    chooser = $('<div />')
	.addClass('chooser')
	.append(img)
    ;
    
    surveyCanvasBack.append(chooser);

    chooser.draggable({
	containment: '.canvas',
	revert: true
    });
}

function surveyChooserApplyWidth() {
    $('.chooser').css('margin-left', -($('.chooser').outerWidth() / 2));
}









/* -- choice -- */

function surveyGenerateChoiceItem(choice)
{
    var choiceItem = false;
    
    if (choice.type == 'image') {
	choiceItem = $('<img />')
	    .attr('src', choice.content.image)
	    .attr('title', choice.content.title)
	    .load(function() {
		surveyChoiceApplySizes();
	    })
	;
    } else if (choice.type == 'text') {
	choiceItem = $('<div />')
	    .addClass('text-choice')
	    .text(choice.content.text)
	;
    }

    return choiceItem;
}

function surveyChoiceShow(choice) {
    choiceDock = $('<div />')
	.addClass('choice-dock')
	.addClass('choice-' + choice.position)
    ;

    choiceDock.data(choice);
    
    choiceDock.append(surveyGenerateChoiceItem(choice));
    
    surveyCanvasBack.append(choiceDock);
    
    choiceDock.droppable({
	hoverClass: "drop-hover",
	tolerance: 'touch',
	greedy: true,
	drop: function(e, ui) {
	    e.stopImmediatePropagation();
	    e.stopPropagation();
	    e.preventDefault();

	    if ($(e.target).closest('.canvas').is('.canvas-front')) {
		surveyChoiceMade($(e.target).data());
	    }
	}
    });
    
    surveyChoiceApplySizes();
}

function surveyChoiceMade(choice) {
    if (typeof(choice.after) != 'undefined') {
	surveyCanvasClear();
	surveyQuestionShow(choice.after);
	currentQuestion = choice.after;
    } else {
	window.location = '/bedankt'
    }
}

function surveyChoiceApplySizes() {
    $('.choice-dock').each(function(i, choice) {
	if ($(choice).hasClass('choice-bottom')) {
	    $(choice).css('margin-left', -($(choice).outerWidth() / 2));
	} else {
	    $(choice).css('margin-top', -($(choice).outerHeight() / 2));
	}
    });
}

module.exports = {
    surveyInit: surveyInit
}
