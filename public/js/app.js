/* globals window */

var form = require('./form.js')

var db = require('../../db/db.js')
var survey = require('./survey.js')

$(document).ready(function() {
    
    // survey

    db.survey(function(error, data) {
	survey.surveyInit('#surveyContainer', data);
    });
    
    // forms
    $('#main-form').submit(function(e) {
	e.preventDefault();
	
	stepData = form.extractStep();
	
	if (form.validateStep(stepData)) {
	    form.submitStep(stepData);
	}
    });
    
    $('#main-form select').change(function(e) {
	hasMoreOptions = $(e.target).find('option:selected').hasClass('more-options');
	if (hasMoreOptions) {
	    $(e.target).siblings('.extra-options').removeClass('hide');
	} else {
	    $(e.target).siblings('.extra-options').addClass('hide');
	}
    });
});

