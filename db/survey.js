'use strict';

var question5 = {
    'background': '/img/map.jpg',
    'text': 'Vraag 3:<br />Op vakantie ga ik het liefst naar',
    'type': 'carousel',
    'choices': [
	{
	    'id': 9,
	    'type': 'image',
	    'content': {
		'image':'/img/icon/netherlands.png',
		'title':'Nederland'
	    }
	},
	{
	    'id': 10,
	    'type': 'image',
	    'content': {
		'image':'/img/icon/germany.png',
		'title':'Duitsland'
	    }
	},
	{
	    'id': 11,
	    'type': 'image',
	    'content': {
		'image':'/img/icon/france.png',
		'title':'Frankrijk'
	    }
	},
	{
	    'id': 12,
	    'type': 'image',
	    'content': {
		'image':'/img/icon/uk.png',
		'title':'Verenigde Koninkrijk'
	    }
	},
	{
	    'id': 13,
	    'type': 'image',
	    'content': {
		'image':'/img/icon/greece.png',
		'title':'Griekenland'
	    }
	}
    ]
};


var question4 = {
    'background': '/img/bounty.png',
    'text': 'Vraag 3:<br />Tijdens mijn vakantie is mijn huis beveiligd met',
    'chooser': {
	'name': 'vakantieganger',
	'image': '/img/poppetje.png'
    },
    'choices': [
	{
	    'id':6,
	    'position':'left',
	    'type':'image',
	    'content': {
		'image':'/img/icon/alarm.png',
		'title':'Alarmsysteem'
	    },
	    'after': question5
	},
	{
	    'id':7,
	    'position':'right',
	    'type':'image',
	    'content': {
		'image':'/img/icon/ccv-camera.png',
		'title':'Buren'
	    },
	    'after': question5
	},
	{
	    'id':8,
	    'position':'bottom',
	    'type':'image',
	    'content': {
		'image':'/img/icon/no-security.png',
		'title':'Niet'
	    },
	    'after': question5
	},
    ]		 
};

var question3 = {
    'background': '/img/bounty.png',
    'text': 'Vraag 2:<br />Ik reis met',
    'chooser': {
	'name': 'vakantieganger',
	'image': '/img/poppetje.png'
    },
    'choices': [
	{
	    'id':6,
	    'position':'left',
	    'type':'image',
	    'content': {
		'image':'/img/icon/plane.png',
		'title':'Vliegtuig'
	    },
	    'after': question4
	},
	{
	    'id':7,
	    'position':'right',
	    'type':'image',
	    'content': {
		'image':'/img/icon/car.png',
		'title':'Auto'
	    },
	    'after': question4
	},
	{
	    'id':8,
	    'position':'bottom',
	    'type':'image',
	    'content': {
		'image':'/img/icon/motor.png',
		'title':'Motor'
	    },
	    'after': question4
	},
    ]
}

var question2 =  {
    'background': '/img/bounty.png',
    //'intro': 'Om te winnen willen we je een paar vragen stellen:',
    'text': 'Vraag 1:<br />Met wie was je op vakantie?',
    'chooser': {
	'name': 'vakantieganger',
	'image': '/img/poppetje.png'
	
    },
    'showProgress': true,
    'choices': [
	{
	    'id':3,
	    'position':'left',
	    'type':'image',
	    'content': {
		'image': '/img/icon/family.png',
		'name': 'Met familie'
	    },
	    'after': question3
	},
	{
	    'id':4,
	    'position':'right',
	    'type':'image',
	    'content': {
		'image': '/img/icon/friends.png',
		'name': 'Met vrienden'
	    },
	    'after': question3
	},
	{
	    'id':5,
	    'position':'bottom',
	    'type':'text',
	    'content': {
		'text':'Anders',
		'needs-extra-text': true
	    },
	    'after': question3
	},
    ]
};


var question1 =  {
    'background': '/img/bounty.png',
    'intro': 'Om te winnen willen we je een paar vragen stellen, maar eerst:',
    'text': 'Ben je een m of v?',
    'chooser': {
	'name': 'vakantieganger',
	'image': '/img/poppetje.png'
	
    },
    'showProgress': true,
    'choices': [
	{
	    'id':3,
	    'position':'left',
	    'type':'image',
	    'content': {
		'image': '/img/icon/male.png',
		'name': 'Man'
	    },
	    'after': question2
	},
	{
	    'id':4,
	    'position':'right',
	    'type':'image',
	    'content': {
		'image': '/img/icon/female.png',
		'name': 'Vrouw'
	    },
	    'after': question2
	}
    ]
};

var survey = {
    surveyId:1,
    step:1,
    totalSteps: 10,
    choices: [],
    
    questions: [
	{
	    'background': '/img/bounty.png',
	    'intro': 'Leuk dat je meedoet!<br />Laten we met het belangrijkste beginnen:',
	    'text': 'Wat wil je winnen?<br /><span class="text-secondary">Swipe de bal en maak een keuze</span>',
	    'chooser': {
                'name': 'bal',
                'image': '/img/ball.png',
                'animation': 'bounce'
	    },
	    'showProgress': false,
	    'choices': [
		{
		    'id' : 1,
		    'position':'right',
		    'type':'image',
		    'content': {
			'image':'img/ipad.png',
			'title':'Win een iPad!'
		    },
		    'after': question1
		},
		{
		    'id':2,
		    'position':'left',
		    'type':'image',
		    'content': {
			'image':'img/anders.png',
			'title':'Win iets anders!'
		    },
		    'after': question1
		}
	    ]
	},
    ]
}

module.exports.survey = function() { return survey; }
