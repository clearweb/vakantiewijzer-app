'use strict';

var path = require("path")
var fs = require("fs")

//var survey_file = path.resolve(__dirname, "survey.js")

module.exports.survey = function (cb) {
    var survey = require("./survey");
    
    return cb(null, survey.survey());
}
