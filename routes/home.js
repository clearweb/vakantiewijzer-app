'use strict';

module.exports = home;

function home (req, res, config) {
    res.template('index.ejs', { title: 'Vakantie-wijzer.nl' });
};
