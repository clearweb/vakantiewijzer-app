'use strict';

module.exports = dataPage;

function dataPage (req, res, config) {
    res.template('data.ejs', { title: 'Vakantie-wijzer.nl' });
};
